<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="styles.css">
  <title>Đăng ký tân sinh viên</title>
  <style>
    body {
            background-color: #f7f7f7;
            height: 100vh;
            margin: 0;
            font-family: 'Times New Roman', Times, sans-serif;
        }

        form {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            width: 90%;
            margin: 0 auto;
        }

        label[for="name"],
        label[for="department"],
        label[for="gender"]{
            font-weight: semibold;
            background-color: cornflowerblue;
            border: 2px solid cornflowerblue;
            border-radius: 3px;
            padding: 10px;
            margin: 8px 0;
            color: white;
            display: block;
            width: 30%;

        }
        span.required {
            color: red;
        }

        .form-group {
            display: flex;
            align-items: center;
        }

        .form-group label {
            margin-right: 20px;
        }

        input[type="text"],
        input[type="password"] {
            flex: 1;
            padding: 10px;
            margin: 8px 0;
            border: 3px solid cornflowerblue;
            border-radius: 3px;
        }

        .radio-group {
            display: flex;
            align-items: center;
        }

        .gender-label {
            color: black;
        }

        input[type="radio"] {
            margin-right: 5px;
            background-color: yellowgreen;
        }

        select {
            padding: 10px;
            margin: 8px 0;
            border: 3px solid cornflowerblue;
            border-radius: 3px;
        }
        button {
          background-color: #669900;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            border: 1px solid cornflowerblue;
            cursor: pointer;
            font-weight: bold;
            margin: 20px auto;
            text-align: center;
            display: block;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
  </style>
</head>
<body>

    <div class="form-container">
    <form id="registrationForm" onsubmit="validateForm(); return false;">
      <div class="form-group">
        <label for="name">Họ và tên</label>
        <input type="text" id="name" name="name">
      </div>
      <div class="form-group">
        <label for="gender">Giới tính</label>
        <input type="radio" id="male" name="gender" value="Nam">
        <label for="male">Nam</label>
        <input type="radio" id="female" name="gender" value="Nữ">
        <label for="female">Nữ</label>
      </div>

      <div class="form-group">
        <label for="department">Phân khoa</label>
        <select id="department" name="department">
          <option value=""> -- Chọn phân khoa -- </option>
          <option value="KHMT">Khoa học máy tính</option>
          <option value="KHVL">Khoa học vật liệu</option>
        </select>
      </div>
      <button type="submit">Đăng ký</button>
    </form>

  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="script.js"></script>
</body>
</html>
