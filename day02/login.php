<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-color: #f7f7f7;
            height: 100%;
            margin: 0;
        }

        p {
            text-align: center;
            background-color: #FFDDCC;
            margin-top: 20px;
        }

        .container {
            background-color: #fff;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            text-align: left;
            max-width: 50%;
            width: 90%;
            margin: 0 auto;
            box-sizing: auto;

        }

        .form-group {
            display: flex;
            align-items: center;

        }

        .form-group label {
            background-color: #279EFF;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            border: 0.5px solid black;
            cursor: pointer;
            font-weight: angular;
            margin: 20px auto;
            text-align: center;
            display: block;
            margin-right: 20px;
        }

        input[type="text"],
        input[type="password"] {
            flex: 1;
            padding: 10px;
            margin: 8px 0;
            border: 1px solid black;
            border-radius: 3px;
        }

        button {
            background-color: #279EFF;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            border: 1px solid black;
            cursor: pointer;
            font-weight: regular;
            margin: 20px auto;
            text-align: center;
            display: block;
            border-bottom-right-radius: 5px;
            border-bottom-left-radius: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
    </style>
</head>

<body>
    <div class="container">
        <?php
        $date_time = new DateTime();
        $time_zone = new DateTimeZone('Asia/Ho_Chi_Minh');

        $date_time->setTimezone($time_zone);

        $thu = $date_time->format('N');
        $thu_dang_chu = ["thứ 2", "thứ 3", "thứ 4", "thứ 5", "thứ 6", "thứ 7", "CN"];

        $ngay = $date_time->format('d');

        $thang = $date_time->format('m');

        $nam = $date_time->format('Y');

        $gio_phut = $date_time->format('H:i');

        echo "<p> Bây giờ là: " . $gio_phut . ", " . $thu_dang_chu[$thu - 1] . " ngày " . $ngay . "/" . $thang . "/" . $nam . "


 </p>";
        ?>
        <form method="POST" action="process_login.php">
            <div class="form-group">
                <label for="username">Tên đăng nhập:</label>
                <input type="text" id="username" name="username" required>
            </div>
            <div class="form-group">
                <label for="password">Mật khẩu:</label>
                <input type="password" id="password" name="password" required>
            </div>
            <button type="submit">Đăng nhập</button>
        </form>
    </div>
</body>

</html>
