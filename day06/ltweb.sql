CREATE DATABASE ltweb;
USE ltweb;
CREATE TABLE students ( id int NOT NULL AUTO_INCREMENT, full_name varchar(255) NOT NULL, gender TINYINT NOT NULL, major varchar(20) NOT NULL, birthday DATETIME NOT NULL, address text, image text, PRIMARY KEY (id) );