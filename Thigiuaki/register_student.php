<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận đăng ký</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="form-container">
        <h1>Xác nhận đăng ký</h1>
        <div id="registrationInfo">
            <p><strong>Họ và tên:</strong> <span id="infoName"></span></p>
            <p><strong>Giới tính:</strong> <span id="infoGender"></span></p>
            <p><strong>Ngày sinh:</strong> <span id="infoDob"></span></p>
            <p><strong>Địa chỉ:</strong> <span id="infoAddress"></span></p>
            <p><strong>Thông tin khác:</strong> <span id="infoOther"></span></p>
        </div>
    </div>

    <script>
        // Lấy thông tin từ URL query string
        const urlParams = new URLSearchParams(window.location.search);
        const name = urlParams.get('name');
        const gender = urlParams.get('gender');
        const dob = urlParams.get('dob');
        const address = urlParams.get('address');
        const other = urlParams.get('other');

        // Hiển thị thông tin đăng ký trên trang submit
        document.getElementById('infoName').textContent = name;
        document.getElementById('infoGender').textContent = gender;
        document.getElementById('infoDob').textContent = dob;
        document.getElementById('infoAddress').textContent = address;
        document.getElementById('infoOther').textContent = other;
    </script>
</body>

</html>
