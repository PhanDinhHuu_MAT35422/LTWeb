<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script>
    function validateForm() {
      const name = document.getElementById('name').value;
    const gender = document.querySelector('input[name="gender"]:checked').value;
    const dob = document.getElementById('year').value + '-' + document.getElementById('month').value + '-' + document.getElementById('date').value;
    const address = document.getElementById('thanhpho').value + ' - ' + document.getElementById('quan').value;
    const other = document.getElementById('information').value;


    if (name.value === "") {
                alert("Hãy nhập họ tên");
                return false;
            }
            else if (gender.value === "") {
              alert("Hay chon gioi tinh");
              return false;
            }
            else if (dob.value === "") {
              alert("Hay chon ngay sinh");
              return false;
            } else if (adress.value === "") {
              alert("Hay chon dia chi");
              return false;
            } else {
              alert("Đăng ký thành công")
              return true;
            }

            window.location.href = `submit.html?name=${name}&gender=${gender}&dob=${dob}&address=${address}&other=${other}`;

}

  </script>
  <title>Form đăng ký sinh viên</title>
</head>

<body>


  <div class="form-container">
  <h1>Form đăng ký sinh viên</h1>
    <form id="registrationForm" onsubmit="validateForm(); return false;">
      <div class="error-messages" id="errorMessages"></div>

      <div class="form-group">
        <label for="name">Họ và tên</label>
        <input type="text" id="name" name="name">
      </div>

      <div class="form-group">
        <label for="gender">Giới tính</label>
        <input type="radio" id="male" name="gender" value="Nam">
        <label for="male">Nam</label>
        <input type="radio" id="female" name="gender" value="Nữ">
        <label for="female">Nữ</label>
      </div>
  </div>
  <div class="form-group">
    <label for="dob">Ngày sinh</label>
    <select id="year" name="year">
      <option value="">Năm</option>
      <!-- Thêm các tùy chọn cho năm từ 1900 đến 2023 (ví dụ) -->
      <script>
        const currentYear = new Date().getFullYear();
        for (let i = 1900; i <= currentYear; i++) {
          document.write(`<option value="${i}">${i}</option>`);
        }
      </script>
    </select>
    <select id="month" name="month">
      <option value="">Tháng</option>
      <!-- Thêm các tùy chọn cho tháng từ 1 đến 12 -->
      <script>
        for (let i = 1; i <= 12; i++) {
          document.write(`<option value="${i}">${i}</option>`);
        }
      </script>
    </select>
    <select id="date" name="date">
      <option value="">Ngày</option>
      <!-- Thêm các tùy chọn cho ngày từ 1 đến 31 -->
      <script>
        for (let i = 1; i <= 31; i++) {
          document.write(`<option value="${i}">${i}</option>`);
        }
      </script>
    </select>
  </div>
  <div class="form-group">
  <label for="adress">Địa chỉ</label>
  <select id="thanhpho" name="thanhpho">
            <option value="">Thành phố</option>
            <option value="hanoi">Hà Nội</option>
            <option value="hochiminh">Hồ Chí Minh</option>
            <!-- Thêm các thành phố khác vào đây -->
        </select>
        <select id="quan" name="quan">
            <option value="">Quận</option>
        </select>

        <script>
            // Định nghĩa các quận tương ứng với từng thành phố
            const quanHanoi = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
            const quanHCM = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];

            const thanhphoInput = document.getElementById("thanhpho");
            const quanInput = document.getElementById("quan");

            thanhphoInput.addEventListener("change", function() {
                // Xóa các tùy chọn quận hiện có
                quanInput.innerHTML = "<option value=''>Chọn quận</option>";

                const selectedThanhPho = thanhphoInput.value;

                // Thêm các tùy chọn quận tương ứng
                let quanList = [];

                if (selectedThanhPho === "hanoi") {
                    quanList = quanHanoi;
                } else if (selectedThanhPho === "hochiminh") {
                    quanList = quanHCM;
                }

                quanList.forEach(function(quan) {
                    quanInput.innerHTML += `<option value="${quan}">${quan}</option>`;
                });
            });
        </script>
  </div>
  <div class="form-group">
    <label for="information">Thông tin khác</label>
    <input type="text" id="information" name="information"/>
  </div>
  <button type="submit" class="Đăng ký">Đăng ký</button>
  </form>

  </div>
</body>

</html>
