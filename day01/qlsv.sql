CREATE DATABASE qlsV

CREATE TABLE dmkhoa (
  MaKH varchar(6) NOT NULL PRIMARY KEY,
  TenKhoa varchar(30) NOT NULL
);

CREATE TABLE sinhvien (
  MaSV varchar(6) NOT NULL PRIMARY KEY,
  HoSV varchar(30) NOT NULL,
  TenSV varchar(15) NOT NULL,
  GioiTinh char(1) NOT NULL,
  NgaySInh datetime NOT NULL,
  NoiSinh varchar(50) NOT NULL,
  DiaChi varchar(50) NOT NULL,
  MaKH varchar(6) NOT NULL,
  HocBong int(11) NOT NULL
);


